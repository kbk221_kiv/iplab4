task1.onclick = function(){
	task1out.innerHTML=ta1();
}
task2.onclick = function(){
	let date1 = new Date();
	task2out.innerHTML=ta2(date1);
}
task3.onclick = function(){
	let n = document.getElementById('daysAgo').value;
	task3out.innerHTML=ta3(Number(n));
}
task4.onclick = function(){
	let y = document.getElementById('YearNeed').value;
  let m = document.getElementById('MonthNeed').value;
	task4out.innerHTML=ta4(Number(y),Number(m)-1);
}
task5.onclick = function(){
  let mas = ta5();
	task5out.innerHTML="Минуло секунд з початку дня: "+mas[0]+"<br> Залишилося секунд до кінця дня:"+mas[1];
}
task6.onclick = function(){
  let da = new Date(document.getElementById('DateON').value);
	task6out.innerHTML=ta6(da);
}
task7.onclick = function(){
  let da1 = new Date(document.getElementById('Da1').value);
  let da2 = new Date(document.getElementById('Da2').value);
  task7out.innerHTML=ta7(da1,da2);
}
task8.onclick = function(){
  let data = new Date(document.getElementById('TimeAgoo').value);
	task8out.innerHTML=formatDate(data);
}
task9.onclick = function(){
  let str1 = document.getElementById('FormDay').value;
  let str2 = document.getElementById('FormMon').value;
  let str3 = document.getElementById('FormYea').value;
	task9out.innerHTML=ta9(str1,str2,str3);
}
task10.onclick = function(){
  let strl = document.getElementById('Language').value;
	task10out.innerHTML=ta10(strl);
}

function ta1(){
	let timee = new Date();
  let allt ="";
  let masmonth = ["січень", "лютий", "березень", "квітень", "травень", "червень", "липень", "серпень","вересень", "жовтень", "листопад", "грудень"]; 
  let masweek = ["неділя","понеділок", "вівторок", "середа", "четвер", "п\'ятниця", "субота"];
  allt +="Дата: "+timee.getDate()+" "+masmonth[timee.getMonth()]+" "+timee.getFullYear()+" року <br>";
  allt +="День: "+masweek[timee.getDay()]+" <br>";
  allt += "Час: "+timee.getHours()+":"+timee.getMinutes()+":"+timee.getSeconds();
  return allt;
}

function ta2(date){
  let datName = ["неділя","понеділок", "вівторок", "середа", "четвер", "п\'ятниця", "субота"];
  let a="";
  (date.getDay()==0)?a+="Номер дня: 7 <br>":a+="Номер дня: "+date.getDay()+"<br>";
  a+="Назва дня: "+datName[date.getDay()];
  return a;
}

function ta3(N){
	let date = new Date();
  let date1 = new Date(date.getFullYear(), date.getMonth(), date.getDate()-N);
  return ""+N+" днів тому було: <br> Дата: "+date1.getDate()+"."+(date1.getMonth()+1)+"."+date1.getFullYear();
}

function ta4(yearn,monthn){
  let data = new Date(yearn,monthn+1,1);
  let data1 = new Date(yearn,data.getMonth(),data.getDate()-1);
  return data1.getDate();
}

function ta5(){
  let data = new Date();
  let rez = new Array();
  rez[0]=data.getHours()*3600+data.getMinutes()*60+data.getSeconds();
  rez[1]=24*3600-rez[0];
  return rez;
}

function ta6(date){
  return ""+date.getDate()+"."+(date.getMonth()+1)+"."+date.getFullYear();
}

function ta7(data1,data2){
  let dy = Math.floor(Math.abs(data2-data1)/(1000*3600*24));
  return ""+dy+" днiв";
}

function formatDate(date){
  let datenow = new Date();
  let x = (datenow-date)/1000;
  if(0<=x&&x<1){
    return "тільки що";
  }else if(1<=x&&x<60){
    return (x + " сек. назад");
  }else if(60<=x&&x<3600){
    return (Math.floor(x/60) +" хв. назад");
  }else{
    return ""+date.getDate()+"."+(date.getMonth()+1)+"."+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes();
  }
}

function ta9(strD,strM,strY){
  let date = new Date(Number(strY),Number(strM)-1,Number(strD));
  let masmonth = ["січень", "лютий", "березень", "квітень", "травень", "червень", "липень", "серпень","вересень", "жовтень", "листопад", "грудень"]; 
  let allform = ""+date.getDate()+" "+masmonth[date.getMonth()]+" "+date.getFullYear()+": ";
  allform += ""+date.getDate()+"."+(date.getMonth()+1)+"."+date.getFullYear()+"<br><br>";
  
  allform+=""+masmonth[date.getMonth()]+" "+date.getDate()+" "+date.getFullYear()+": ";
  allform+=""+(date.getMonth()+1)+"."+date.getDate()+"."+date.getFullYear()+"<br><br>";
  
  allform+=""+date.getFullYear()+" "+masmonth[date.getMonth()]+" "+date.getDate()+": ";
  allform+=""+date.getFullYear()+"."+(date.getMonth()+1)+"."+date.getDate();
  return allform;
}

function ta10(lang){
  let date = new Date();
  let options = {
    era: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    weekday: 'long',
    timezone: 'UTC',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric'
  };
  if(lang=="en"){
    return date.toLocaleString("en-US", options);
  }else if(lang=="uk"){
    return date.toLocaleString("uk-UK", options); 
  }else{
    return "ви написали неправильно, спробуйте ще раз";
  }
}